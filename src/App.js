import './App.css';
import React from 'react';
import { BrowserRouter as Browser, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar/Navbar';
import logo from './images/logo.png'
import Home from './components/Home';

class App extends React.Component {
  render() {
    const nav = [
      {
        name: 'Kitchen',
        link: '/'
      },
      {
        name: 'Bathrooms',
        link: '/'
      },
      {
        name: 'Windows & Doors',
        link: '/'
      },
      {
        name: 'Glazed Extension',
        link: '/'
      },
      {
        name: 'Login',
        link: '/'
      },
      {
        name: 'Register',
        link: '/'
      }
    ];
    return (
      <>
        <Browser>
          <Navbar
            logo={logo}
            activeNav={{ name: 'Home', link: '/' }}
            nav={nav}
          />
          <Routes>
            <Route path='/' element={<Home />} />
          </Routes>
        </Browser>
      </>
    );
  }
}

export default App;
