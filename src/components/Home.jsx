import React, { Component } from 'react'
import Carousel from './Carousel/Carousel';
import MainContent from './Maincontent/MainContent';
import Portfolio from './Portfloio/Portfolio';
import ContactUs from './ContactUs/ContactUs';

class Home extends Component {
    render() {
        return (
            <>
                <Carousel time={5000} />
                <MainContent />
                <Portfolio />
                <ContactUs
                    address='115 Marryland Avenue - Birmingham - B34 6ED'
                    phoneNumber='+44-7983-886045'
                    email="rayeesmpazir@icloud.com"
                />
            </>
        )
    }
}

export default Home;