import React, { Component } from 'react'

class ContactUs extends Component {
    render() {
        return (
            <>
                <div className="container my-5">
                    <h1 className='heading'>Contact Us</h1>
                    <p className='para text-start'>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
                    <div className='d-flex flex-wrap justify-content-center my-5'>
                        <div className='fs-1 d-flex flex-column justify-content-center align-items-center px-5 border-end'>
                            <i className="my-3 fa-solid fa-location-dot text-success"></i>
                            <h2 className='text-secondary'>Address</h2>
                            <p className='fs-6 text-center'>{this.props.address}</p>
                        </div>
                        <div className='fs-1 d-flex flex-column justify-content-center align-items-center px-5 border-end'>
                            <i className="my-3 fa-solid fa-phone text-success"></i>
                            <h2 className='text-secondary'>Phone Number</h2>
                            <p className='fs-6 text-center'>{this.props.phoneNumber}</p>
                        </div>
                        <div className='fs-1 d-flex flex-column justify-content-center align-items-center px-5'>
                            <i className="my-3 fa-solid fa-envelope text-success"></i>
                            <h2 className='text-secondary'>Email</h2>
                            <p className='fs-6 text-center'>{this.props.email}</p>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default ContactUs;