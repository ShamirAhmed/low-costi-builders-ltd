import React, { Component } from 'react'

class UpperNav extends Component {
    render() {
        return (
            <div>
                <div className='container'>
                    <div className='row p-2'>
                        <div className='col-12 col-md-6 d-flex flex-row justify-content-around'>
                            <div className='mx-3'>
                                <i className="fa-solid fa-envelope px-2 text-success"></i>
                                <span>{this.props.email}</span>
                            </div>
                            <div className='mx-3'>
                                <i className="fa-solid fa-mobile-screen-button px-2 text-success"></i>
                                <span>{this.props.mobileNumber}</span>
                            </div>
                        </div>
                        <div className='col-12 col-md-6 flex-row align-items-center justify-content-end d-none d-md-flex'>
                            <i className="fa-brands fa-instagram px-3 border-end hover"></i>
                            <i className="fa-brands fa-facebook-f px-3 border-end"></i>
                            <i className="fa-brands fa-linkedin px-3 border-end"></i>
                            <i className="fa-brands fa-twitter px-3"></i>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UpperNav