import React, { Component } from 'react'
import { Link } from 'react-router-dom';

import './navbar.css'
import { v4 as uuidv4 } from 'uuid';
import UpperNav from './UpperNav';

export class Navbar extends Component {
    render() {
        return (
            <>
                <UpperNav
                    email='rayeesmpazir@icloud.com'
                    mobileNumber='+44-7983-886045'
                />
                <div className='sticky-top'>
                    <nav className="navbar navbar-expand-lg bg-light shadow rounded">
                        <div className="container-fluid">
                            <Link className="navbar-brand" to='/'>
                                <img src={this.props.logo} alt='logo' className='logo'></img>
                            </Link>
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarNav">
                                <ul className="navbar-nav ms-auto">
                                    <li className="nav-item">
                                        <Link className="nav-link active nav-heading" aria-current="page" to={this.props.activeNav.link}>{this.props.activeNav.name}</Link>
                                    </li>
                                    {
                                        this.props.nav
                                            .map((each) => {
                                                return (
                                                    <li className="nav-item" key={uuidv4()}>
                                                        <Link className="nav-link nav-heading" to={each.link}>{each.name}</Link>
                                                    </li>
                                                );
                                            })
                                    }
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </>
        )
    }
}

export default Navbar