import React, { Component } from 'react'
import './features.css'

export class Features extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         
      }
    }
    render() {
        return (
            <div>
                <div className='container'>
                    <div className={`row d-flex ${this.props.align} justify-content-around p-5`}>
                        <div className='col-12 col-md-6'>
                            <img src={this.props.image} alt='' className='image'></img>
                        </div>
                        <div className='col-12 col-md-6 d-flex flex-column justify-content-start align-items-start'>
                            <h1 className='heading mb-3'>{this.props.heading}</h1>
                            <p className='sub-heading mb-3'>{this.props.subHeading}</p>
                            <p className='para text-start mb-3'>{this.props.para}</p>
                            <div>
                                <button className='btn btn-outline-primary'>Learn More</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Features