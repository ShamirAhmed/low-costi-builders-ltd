import React, { Component } from 'react'
import Features from './Features/Features'
import bathroom from '../../images/bathroom.jpg'

export class MainContent extends Component {
  render() {
    return (
      <div>
        <Features
            heading='Luxury & Bespoke'
            subHeading='Kitchens'
            para='At Dobsons we believe the kitchen is the heart of the home, a place for cooking, eating entertaining, work and play, a place you will love forever and that will stand up to the rigours of modern life.'
            image={bathroom}
            align='flex-row'
        >
        </Features>
        <Features
            heading='Beautiful & Bespoke'
            subHeading='Bathrooms'
            para='Our comprehensive bathroom design and installation service takes care of everything for you. It begins with listening to your needs and working with you to make the most of your space.'
            image={bathroom}
            align='flex-row-reverse'
        >
        </Features>
        <Features
            heading='Distinctive'
            subHeading='Windows & Doors'
            para='Double glazing has been part of our business for nearly 40 years during which time we have consistently installed only the best products using our own employed installers.'
            image={bathroom}
        >
        </Features>
        <Features
            heading='Unique'
            subHeading='Glazed Extensions'
            para='Our contemporary glazed extensions offer an exciting and inspiring way to increase the size of your home and provide that much needed extra room.'
            image={bathroom}
            align='flex-row-reverse'
        >
        </Features>
      </div>
    )
  }
}

export default MainContent