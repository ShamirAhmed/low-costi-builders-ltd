import React, { Component } from 'react'

class Buttons extends Component {
    render() {
        return (
            <>
                <button id={this.props.name.toLowerCase()} type="button" className={`btn btn${this.props.active?'-':'-outline-'}success mx-2 my-5`} onClick={this.props.filter}>{this.props.name}</button>
            </>
        )
    }
}

export default Buttons;