import React, { Component } from 'react'
import Buttons from './Buttons';
import Cards from './Cards';
import p3 from '../../images/p3.jpeg';

class Portfolio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            all: true,
            kitchens: false,
            doors: false,
            bathrooms: false,
        }
    }

    filter = (event) => {
        this.setState({
            all: false,
            kitchens: false,
            doors: false,
            bathrooms: false,
            [event.target.id]: !this.state[event.target.id]
        })
    }

    render() {
        return (
            <>
                <div className="container">
                    <h1 className="heading">OUR PORTFOLIO</h1>
                    <p className='para text-start'>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
                    <div className='d-flex justify-content-center'>
                        <Buttons name='All' active={this.state.all} filter={this.filter} />
                        <Buttons name='Kitchens' active={this.state.kitchens} filter={this.filter} />
                        <Buttons name='Doors' active={this.state.doors} filter={this.filter} />
                        <Buttons name='Bathrooms' active={this.state.bathrooms} filter={this.filter} />
                    </div>
                    <div className='d-flex row justify-content-center'>
                        <Cards image={p3} type='Kitchens' filter={this.state} />
                        <Cards image={p3} type='Bathrooms' filter={this.state} />
                        <Cards image={p3} type='Kitchens' filter={this.state} />
                        <Cards image={p3} type='Doors' filter={this.state} />
                        <Cards image={p3} type='Doors' filter={this.state} />
                        <Cards image={p3} type='Doors' filter={this.state} />
                    </div>
                </div>
            </>
        )
    }
}

export default Portfolio;