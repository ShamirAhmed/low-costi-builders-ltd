import React, { Component } from 'react'

class Cards extends Component {
    render() {
        const compair = this.props.filter['all'] ? true : this.props.filter[this.props.type.toLowerCase()]
        return (
            <>
                <div className={`col-12 col-md-4 my-3 ${compair ? '' : 'd-none'}`}>
                    <img src={this.props.image} alt='' className='image'></img>
                </div>
            </>
        )
    }
}

export default Cards;