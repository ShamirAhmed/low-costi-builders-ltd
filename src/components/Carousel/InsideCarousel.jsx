import React, { Component } from 'react'

class InsideCarousel extends Component {
    render() {
        return (
            <>
                <div className='container d-flex justify-content-center align-items-center flex-column h-100'>
                    <h1 className='text-center inside-carousel'>
                        <span style={{ color: '#0c2e8a' }}>
                            Making <span style={{ color: '#50d8af' }}>
                                your ideas
                            </span> Happen!
                        </span>
                    </h1>
                    <div>
                        <button type="button" className="btn btn-outline-primary --bs-white me-5">Get Started</button>
                        <button type="button" className="btn btn-outline-primary">Our Projects</button>
                    </div>
                </div>
            </>
        )
    }
}

export default InsideCarousel;