import React, { Component } from 'react'
import './Carousel.css';
import InsideCarousel from './InsideCarousel';

class Carousel extends Component {
    render() {
        return (
            <>
                <div id="carouselExampleInterval" className="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active bg-image" data-bs-interval={this.props.time}>
                            <div className='mybg' style={{ "--background": 'url(https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/71nqaV173zL._SX3000_.jpg)' }}>
                                <InsideCarousel />
                            </div>
                        </div>
                        <div className="carousel-item" data-bs-interval={this.props.time}>
                            <div className='mybg' style={{ "--background": 'url(https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61AQSNQQBaL._SX3000_.jpg)' }}>
                                <InsideCarousel />
                            </div>
                        </div>
                        <div className="carousel-item" data-bs-interval={this.props.time}>
                            <div className='mybg' style={{ "--background": 'url(https://m.media-amazon.com/images/W/WEBP_402378-T1/images/I/61gk2vNLsbL._SX3000_.jpg)' }}>
                                <InsideCarousel />
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Carousel;